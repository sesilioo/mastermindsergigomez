package mastermind;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JugarTest {

    @Test
    void testCalcularEncerts() {
        Jugar jugar = new Jugar();

        
        int[] combinacionUsuario1 = {1, 2, 3, 4};
        int[] combinacionPrograma1 = {5, 6, 7, 8};
        assertEquals(0, jugar.calcularEncerts(combinacionUsuario1, combinacionPrograma1));

        
        int[] combinacionUsuario2 = {4, 3, 2, 1};
        int[] combinacionPrograma2 = {1, 2, 3, 4};
        assertEquals(4, jugar.calcularEncerts(combinacionUsuario2, combinacionPrograma2));

        
        int[] combinacionUsuario3 = {5, 2, 3, 8};
        int[] combinacionPrograma3 = {1, 2, 3, 4};
        assertEquals(2, jugar.calcularEncerts(combinacionUsuario3, combinacionPrograma3));

       
        int[] combinacionUsuario4 = {9, 8, 7, 6};
        int[] combinacionPrograma4 = {1, 2, 3, 4};
        assertEquals(0, jugar.calcularEncerts(combinacionUsuario4, combinacionPrograma4));
        
        int[] combinacionUsuario5 = {1, 2, 3, 4};
        int[] combinacionPrograma5 = {1, 2, 3, 4};
        assertEquals(4, jugar.calcularEncerts(combinacionUsuario5, combinacionPrograma5));

        
    }

    @Test
    void testContieneNumero() {
        Jugar jugar = new Jugar();

        int[] array1 = {1, 2, 3, 4};
        assertTrue(jugar.contieneNumero(array1, 3));

        
        int[] array2 = {5, 6, 7, 8};
        assertFalse(jugar.contieneNumero(array2, 9));

        
        int[] array3 = {2, 3, 5, 2};
        assertTrue(jugar.contieneNumero(array3, 2));

        
    }
}
