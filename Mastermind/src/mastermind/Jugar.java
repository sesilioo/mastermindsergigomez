package mastermind;

import java.util.Random;
import java.util.Scanner;

/**
 * Clase que representa el juego Mastermind y contiene los métodos necesarios para jugar.
 *
 * @version 1.0
 * @author Sergi Gomez
 */
public class Jugar {
    // Variables para almacenar estadísticas de las partidas jugadas.
    int partidesGuanyades;
    int partidesPerdudes;
    int partidesNoFinalitzades;

    /**
     * Constructor de la clase Jugar.
     */
    public Jugar() {
    }

    /**
     * Genera una combinación aleatoria de 4 números entre 0 y 9 sin repeticiones.
     *
     * @return La combinación generada.
     */
    public int[] generarCombinacio() {
        Random rnd = new Random();
        int[] combinacio = new int[4];
        boolean[] numerosUtilitzats = new boolean[10];

        for (int i = 0; i < 4; i++) {
            int num;
            do {
                num = rnd.nextInt(10);
            } while (numerosUtilitzats[num]);
            combinacio[i] = num;
            numerosUtilitzats[num] = true;
        }

        return combinacio;
    }

    /**
     * Inicia y juega una partida de Mastermind.
     */

    public void jugarPartida() {
        int[] combinacioPrograma = generarCombinacio();
        int jugades = 0;
        boolean partidaFinalitzada = false;
        Scanner scanner = new Scanner(System.in);

        while (!partidaFinalitzada && jugades < 15) {
            System.out.print("Introdueix la teva combinació (4 números entre 0 i 9 sense repeticions): ");
            int[] combinacioUsuari = llegirCombinacio(scanner);

            int encerts = calcularEncerts(combinacioUsuari, combinacioPrograma);

            if (encerts == 4) {
                partidaFinalitzada = true;
                partidesGuanyades++;
                System.out.println("Enhorabona!! Has guanyat la partida esbrinant la combinació en " + (jugades + 1) + " intents.");
            } else {
                System.out.println("Encerts: " + encerts);
                jugades++;
                System.out.println("Tens " + (15 - jugades) + " intents restants.");
                System.out.print("Vols continuar jugant? (S/N): ");
                String resposta = scanner.next();
                if (resposta.equalsIgnoreCase("N")) {
                    partidaFinalitzada = true;
                    partidesNoFinalitzades++;
                    System.out.println("Ops! Has deixat la partida sense acabar.");
                }
            }
        }

        if (!partidaFinalitzada) {
            partidesPerdudes++;
            System.out.println("Ooohhh! Has perdut. La combinació que buscaves és: " + combinacioPrograma[0] + combinacioPrograma[1] + combinacioPrograma[2] + combinacioPrograma[3]);
        }
    }

    /**
     * Muestra los resultados acumulados de las partidas jugadas.
     */
    public void mostrarResultatsAcumulats() {
        System.out.println("Resultats acumulats:");
        System.out.println("Partides guanyades: " + partidesGuanyades);
        System.out.println("Partides perdudes: " + partidesPerdudes);
        System.out.println("Partides no finalitzades: " + partidesNoFinalitzades);
    }

    /**
     * Lee la combinación introducida por el usuario.
     *
     * @param scanner El objeto Scanner para leer la entrada del usuario.
     * @return La combinación introducida por el usuario.
     */
    private int[] llegirCombinacio(Scanner scanner) {
        int[] combinacio = new int[4];
        boolean[] numerosUtilitzats = new boolean[10];

        for (int i = 0; i < 4; i++) {
            boolean numeroValid = false;
            do {
                if (scanner.hasNextInt()) {
                    int num = scanner.nextInt();
                    if (num >= 0 && num <= 9 && !numerosUtilitzats[num]) {
                        combinacio[i] = num;
                        numerosUtilitzats[num] = true;
                        numeroValid = true;
                    } else {
                        System.out.println("Error: Introdueix un número vàlid (0-9) que no hagi estat utilitzat abans.");
                    }
                } else {
                    System.out.println("Error: Introdueix un número vàlid (0-9) que no hagi estat utilitzat abans.");
                    scanner.next(); // Consumir entrada incorrecta
                }
            } while (!numeroValid);
        }

        return combinacio;
    }

    /**
     * Calcula los aciertos en la combinación del usuario comparada con la del programa.
     *
     * @param combinacioUsuari    La combinación introducida por el usuario.
     * @param combinacioPrograma  La combinación generada por el programa.
     * @return La cantidad de aciertos.
     */
    int calcularEncerts(int[] combinacioUsuari, int[] combinacioPrograma) {
        int encerts = 0;

        for (int i = 0; i < 4; i++) {
            if (combinacioUsuari[i] == combinacioPrograma[i]) {
                encerts++;
            } else if (contieneNumero(combinacioPrograma, combinacioUsuari[i])) {
                encerts++;
            }
        }

        return encerts;
    }


    /**
     * Verifica si un número está presente en la combinación dada.
     *
     * @param array  La combinación en la que buscar.
     * @param numero El número a buscar.
     * @return true si el número está presente, false en caso contrario.
     */
    boolean contieneNumero(int[] array, int numero) {
        for (int i : array) {
            if (i == numero) return true;
        }
        return false;
    }

}
