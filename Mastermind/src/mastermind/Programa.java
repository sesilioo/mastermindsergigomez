package mastermind;

import java.util.Scanner;

/**
 * Clase principal que contiene el método main para ejecutar el juego Mastermind.
 * 
 * @version 1.0
 * @author Sergi Gomez
 */
public class Programa {
    /**
     * Método principal que inicia el juego y muestra el menú.
     *
     * @param args Los argumentos de la línea de comandos (no se utilizan).
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Jugar jugar = new Jugar();

        // Variable para almacenar la opción del menú seleccionada por el usuario.
        int opcio;

        // Ciclo principal del juego.
        do {
            // Menú de opciones.
            System.out.println("Menú:");
            System.out.println("1. Generar combinació");
            System.out.println("2. Jugar");
            System.out.println("3. Resultats acumulats");
            System.out.println("0. Sortir");
            System.out.print("Introdueix una opció: ");

            // Lee la opción del usuario.
            opcio = scanner.nextInt();

            // Realiza acciones según la opción seleccionada.
            switch (opcio) {
                case 1:
                    System.out.println("Combinació generada.");
                    break;
                case 2:
                    jugar.jugarPartida();
                    break;
                case 3:
                    // Muestra los resultados acumulados.
                    if (jugar != null) {
                        jugar.mostrarResultatsAcumulats();
                    } else {
                        System.out.println("Error: Abans de mostrar resultats, cal generar una combinació.");
                    }
                    break;
                case 0:
                    System.out.println("Adeu! Fins la propera.");
                    break;
                default:
                    System.out.println("Error: Opció no vàlida. Torna a intentar-ho.");
            }
        } while (opcio != 0);
    }
}
